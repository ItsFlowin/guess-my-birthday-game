from random import randint

name = input('Hi! What is your name? \n')


for tries in range(1,6):

    rand_month = randint(1,12)
    rand_year = randint(1924,2004)

 

    print('Guess', tries, ': ' + name + ' were you born in ', rand_month , '/' , rand_year ,'?')
    answer = input('Yes or No?\n')
    
    # If yes than exit    
    if (answer == 'yes'):
        print('I Knew it!')
        exit()
    elif (tries == 5):
        print('I have other things to do. Good Bye')
    else:
        print('Drat! Lemme try again')